/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.modules.test.dao;

import com.wolfking.jeesite.common.persistence.CrudDao;
import com.wolfking.jeesite.common.persistence.annotation.MyBatisDao;
import com.wolfking.jeesite.modules.test.entity.TestDataChild;

/**
 * 主子表生成DAO接口
 * @author ThinkGem
 * @version 2018-10-14
 */
@MyBatisDao
public interface TestDataChildDao extends CrudDao<TestDataChild> {
	
}