/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.modules.phone.frontContrller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wolfking.jeesite.common.web.BaseController;

/**
 * 主子表生成Controller
 * @author ThinkGem
 * @version 2018-10-14
 */
@Controller
@RequestMapping(value = "web")
public class FrontController extends BaseController {
	
	@RequestMapping("/{path}")
	public String share(@PathVariable("path") String path){
		return "phone/"+path;
	}

}