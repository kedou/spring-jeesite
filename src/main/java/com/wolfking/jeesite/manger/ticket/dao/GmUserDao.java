/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.jeesite.common.persistence.CrudDao;
import com.wolfking.jeesite.manger.ticket.entity.GmUser;

/**
 * 单表生成DAO接口
 * @author 张瑞林
 * @version 2018-10-19
 */
@Mapper
public interface GmUserDao extends CrudDao<GmUser> {

	GmUser findByLoginName(String loginName);
	
}