/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.entity;

import com.wolfking.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

import com.wolfking.jeesite.common.persistence.DataEntity;

/**
 * 单表生成Entity
 * @author 张瑞林
 * @version 2018-10-19
 */
public class GmWeixinHistory extends DataEntity<GmWeixinHistory> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 所属用户
	private String nicheng;		// 昵称
	private String totalPrice;		// 成交额
	private String chengjiao;		// 成交状态：1：失败  2：成功
	private String status;		// 处理状态 1:未处理  2：已处理
	private String weixinNumber;		// 微信号
	
	public GmWeixinHistory() {
		super();
	}

	public GmWeixinHistory(String id){
		super(id);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Length(min=1, max=100, message="昵称长度必须介于 1 和 100 之间")
	public String getNicheng() {
		return nicheng;
	}

	public void setNicheng(String nicheng) {
		this.nicheng = nicheng;
	}
	
	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Length(min=0, max=255, message="成交状态：1：失败  2：成功长度必须介于 0 和 255 之间")
	public String getChengjiao() {
		return chengjiao;
	}

	public void setChengjiao(String chengjiao) {
		this.chengjiao = chengjiao;
	}
	
	@Length(min=0, max=6, message="处理状态 1:未处理  2：已处理长度必须介于 0 和 6 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Length(min=1, max=100, message="微信号长度必须介于 1 和 100 之间")
	public String getWeixinNumber() {
		return weixinNumber;
	}

	public void setWeixinNumber(String weixinNumber) {
		this.weixinNumber = weixinNumber;
	}
	
}