/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wolfking.jeesite.common.config.Global;
import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.web.BaseController;
import com.wolfking.jeesite.common.utils.StringUtils;
import com.wolfking.jeesite.manger.ticket.entity.GmUser;
import com.wolfking.jeesite.manger.ticket.service.GmUserService;

/**
 * 单表生成Controller
 * @author 张瑞林
 * @version 2018-10-19
 */
@Controller
@RequestMapping(value = "${adminPath}/ticket/gmUser")
public class GmUserController extends BaseController {

	@Autowired
	private GmUserService gmUserService;
	
	@ModelAttribute
	public GmUser get(@RequestParam(required=false) String id) {
		GmUser entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = gmUserService.get(id);
		}
		if (entity == null){
			entity = new GmUser();
		}
		return entity;
	}
	
	@RequiresPermissions("ticket:gmUser:view")
	@RequestMapping(value = {"list", ""})
	public String list(GmUser gmUser, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<GmUser> page = gmUserService.findPage(new Page<GmUser>(request, response), gmUser); 
		model.addAttribute("page", page);
		return "manger/ticket/gmUserList";
	}

	@RequiresPermissions("ticket:gmUser:view")
	@RequestMapping(value = "form")
	public String form(GmUser gmUser, Model model) {
		model.addAttribute("gmUser", gmUser);
		return "manger/ticket/gmUserForm";
	}

	@RequiresPermissions("ticket:gmUser:edit")
	@RequestMapping(value = "save")
	public String save(GmUser gmUser, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, gmUser)){
			return form(gmUser, model);
		}
		gmUserService.save(gmUser);
		addMessage(redirectAttributes, "保存单表成功");
		return "redirect:"+Global.getAdminPath()+"/ticket/gmUser/?repage";
	}
	
	@RequiresPermissions("ticket:gmUser:edit")
	@RequestMapping(value = "delete")
	public String delete(GmUser gmUser, RedirectAttributes redirectAttributes) {
		gmUserService.delete(gmUser);
		addMessage(redirectAttributes, "删除单表成功");
		return "redirect:"+Global.getAdminPath()+"/ticket/gmUser/?repage";
	}

}