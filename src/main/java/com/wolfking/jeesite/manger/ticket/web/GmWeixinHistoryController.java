/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wolfking.jeesite.common.config.Global;
import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.web.BaseController;
import com.wolfking.jeesite.common.utils.StringUtils;
import com.wolfking.jeesite.manger.ticket.entity.GmWeixinHistory;
import com.wolfking.jeesite.manger.ticket.service.GmWeixinHistoryService;

/**
 * 单表生成Controller
 * @author 张瑞林
 * @version 2018-10-19
 */
@Controller
@RequestMapping(value = "${adminPath}/ticket/gmWeixinHistory")
public class GmWeixinHistoryController extends BaseController {

	@Autowired
	private GmWeixinHistoryService gmWeixinHistoryService;
	
	@ModelAttribute
	public GmWeixinHistory get(@RequestParam(required=false) String id) {
		GmWeixinHistory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = gmWeixinHistoryService.get(id);
		}
		if (entity == null){
			entity = new GmWeixinHistory();
		}
		return entity;
	}
	
	@RequiresPermissions("ticket:gmWeixinHistory:view")
	@RequestMapping(value = {"list", ""})
	public String list(GmWeixinHistory gmWeixinHistory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<GmWeixinHistory> page = gmWeixinHistoryService.findPage(new Page<GmWeixinHistory>(request, response), gmWeixinHistory); 
		model.addAttribute("page", page);
		return "manger/ticket/gmWeixinHistoryList";
	}

	@RequiresPermissions("ticket:gmWeixinHistory:view")
	@RequestMapping(value = "form")
	public String form(GmWeixinHistory gmWeixinHistory, Model model) {
		model.addAttribute("gmWeixinHistory", gmWeixinHistory);
		return "manger/ticket/gmWeixinHistoryForm";
	}

	@RequiresPermissions("ticket:gmWeixinHistory:edit")
	@RequestMapping(value = "save")
	public String save(GmWeixinHistory gmWeixinHistory, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, gmWeixinHistory)){
			return form(gmWeixinHistory, model);
		}
		gmWeixinHistoryService.save(gmWeixinHistory);
		addMessage(redirectAttributes, "保存单表成功");
		return "redirect:"+Global.getAdminPath()+"/ticket/gmWeixinHistory/?repage";
	}
	
	@RequiresPermissions("ticket:gmWeixinHistory:edit")
	@RequestMapping(value = "delete")
	public String delete(GmWeixinHistory gmWeixinHistory, RedirectAttributes redirectAttributes) {
		gmWeixinHistoryService.delete(gmWeixinHistory);
		addMessage(redirectAttributes, "删除单表成功");
		return "redirect:"+Global.getAdminPath()+"/ticket/gmWeixinHistory/?repage";
	}

}