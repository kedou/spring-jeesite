/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.service.CrudService;
import com.wolfking.jeesite.manger.ticket.entity.GmUser;
import com.wolfking.jeesite.manger.ticket.dao.GmUserDao;

/**
 * 单表生成Service
 * @author 张瑞林
 * @version 2018-10-19
 */
@Service
@Transactional(readOnly = true)
public class GmUserService extends CrudService<GmUserDao, GmUser> {
	@Autowired
	private GmUserDao gmUserDao;
	
	public GmUser get(String id) {
		return super.get(id);
	}
	
	public List<GmUser> findList(GmUser gmUser) {
		return super.findList(gmUser);
	}
	
	public Page<GmUser> findPage(Page<GmUser> page, GmUser gmUser) {
		return super.findPage(page, gmUser);
	}
	
	@Transactional(readOnly = false)
	public void save(GmUser gmUser) {
		super.save(gmUser);
	}
	
	@Transactional(readOnly = false)
	public void delete(GmUser gmUser) {
		super.delete(gmUser);
	}

	public GmUser findByLoginName(String loginName) {
		return gmUserDao.findByLoginName(loginName);
	}
	
}