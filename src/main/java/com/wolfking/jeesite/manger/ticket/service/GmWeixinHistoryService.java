/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.manger.ticket.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.service.CrudService;
import com.wolfking.jeesite.manger.ticket.entity.GmWeixinHistory;
import com.wolfking.jeesite.manger.ticket.dao.GmWeixinHistoryDao;

/**
 * 单表生成Service
 * @author 张瑞林
 * @version 2018-10-19
 */
@Service
@Transactional(readOnly = true)
public class GmWeixinHistoryService extends CrudService<GmWeixinHistoryDao, GmWeixinHistory> {

	public GmWeixinHistory get(String id) {
		return super.get(id);
	}
	
	public List<GmWeixinHistory> findList(GmWeixinHistory gmWeixinHistory) {
		return super.findList(gmWeixinHistory);
	}
	
	public Page<GmWeixinHistory> findPage(Page<GmWeixinHistory> page, GmWeixinHistory gmWeixinHistory) {
		return super.findPage(page, gmWeixinHistory);
	}
	
	@Transactional(readOnly = false)
	public void save(GmWeixinHistory gmWeixinHistory) {
		super.save(gmWeixinHistory);
	}
	
	@Transactional(readOnly = false)
	public void delete(GmWeixinHistory gmWeixinHistory) {
		super.delete(gmWeixinHistory);
	}
	
}